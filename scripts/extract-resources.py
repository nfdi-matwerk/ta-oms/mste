import markdown2


def main():
    #http = urllib3.PoolManager()
    #response = http.request('GET', "https://git.rwth-aachen.de/nfdi-matwerk/ta-oms/mste/-/raw/main/README.md")
    #text = response.data.decode('utf-8')
    with open("README.md", "r") as file:
        text = file.read()


    text_intro="Do you have a schema that you would like to share?</br> Please visit the <a href=\"https://git.rwth-aachen.de/nfdi-matwerk/ta-oms/mste\">NFDI MatWerk Schema and Terminology Exchange</a> to see how your resource can be added.</br>"
    text_split_intro = text.split("description/keywords.", 1)
    text_split_ack = text_split_intro[1].split("## Acknowledgements", 1)
    converter = markdown2.Markdown(extras=["tables"])  # <-- here
    html = converter.convert(text_split_ack[0])
    html=text_intro+html

    with open('resources.html', 'w') as f:
        f.write(html)

if __name__ == "__main__":
    main()