# MatWerk Schemas and Terminology Exchange

![MSTE](TAOMS_STxCHANGE_header.png)

---
Welcome to the NFDI MatWerk Schemas & Terminology Exchange :wave: 

:point_right: This repository lists semantic resources including (1) metadata schemas, (2) specifications, (3) (semantic) data formats, (4) semantic artefacts, (5) further resources, and (6) subject agnostic resources that are relevant and used in the field of Material Science and Engineering. :point_left: 


## Contributions are welcome :rocket:

Would you like to add a MSE-related schema? Do you know of some relevant resources in the field? 

Please feel free to contribute to this [repository](https://git.rwth-aachen.de/nfdi-matwerk/ta-oms/mste) by creating an [issue](https://git.rwth-aachen.de/nfdi-matwerk/ta-oms/mste/-/issues/new) including name of the resource, links to the repository and documentation as well as a brief description/keywords.

You can also contact us at [MatWerkOMS@fz-juelich.de](mailto:MatWerkOMS@fz-juelich.de)

## Metadata Schemas

|Name &#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;|Repository|Documentation|Keywords&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;|
|:---|:---:|:---:|:---|
| MWO metadata template for datasets | [:file_folder:](https://git.rwth-aachen.de/nfdi-matwerk/ta-oms/mste/-/tree/main/MWO%20templates/dataset) |   | Resource description, MSE KG, YAML|
| MWO metadata template for experimental workflows | [:file_folder:](https://git.rwth-aachen.de/nfdi-matwerk/ta-oms/mste/-/tree/main/MWO%20templates/experimentalWorkflow) |   | Resource description, MSE KG, YAML|
| MWO metadata template for computational workflows | [:file_folder:](https://git.rwth-aachen.de/nfdi-matwerk/ta-oms/mste/-/tree/main/MWO%20templates/computationalWorkflow) |   | Resource description, MSE KG, YAML|
| Metadata schemas for materials science data                          | [:file_folder:](https://zenodo.org/record/6513745#.YvS4gNJBwUH)                                                                                                   |                                                 | JSON, characterization, simulation                                   |
| Web Standards and Reference Data for First-Principles Simulations    |                                                                                                                                                  | [:bookmark_tabs:](http://www.quantum-simulation.org/)              | XML, atomistic simulations                                           |
| EngMeta                                                              |                                                                                                                                                  | [:bookmark_tabs:](https://www.izus.uni-stuttgart.de/fokus/engmeta) | XSD, engineering                                                     |
| Essential Source of Schemas and Examples (ESSE)                      | [:file_folder:](https://github.com/Exabyte-io/esse)                                                                                                               |                                                 | JSON, structural data, characteristic properties, modeling workflows |
| MGI JSON Schema                                                      | [:file_folder:](https://github.com/usnistgov/mgi-json-schema)                                                                                                     |                                                 | JSON                                                                 |
| Schema for scanning electron microscopy (SEM) | [:file_folder:](https://matwerk.datamanager.kit.edu/api/v1/schemas/scanning-electron-microscopy)                                         |                                                 | JSON, SEM                                                            |
| Schema for transmission electron microscopy (TEM) | [:file_folder:](https://matwerk.datamanager.kit.edu/api/v1/schemas/tem)                                         |                                                 | JSON, TEM                                                            |
| Schema for magnetic resonance imaging (MRI) | [:file_folder:](https://matwerk.datamanager.kit.edu/api/v1/schemas/mri_schema)                                         |                                                 | JSON, MRI                                                            |
| Schemas for FIB/SEM serial sectioning tomography | [:file_folder:](https://matwerk.datamanager.kit.edu/api/v1/schemas/sem-fib-tomography-acquisition) |                                                 | JSON, FIB/SEM                                                        |

## Specifications

|Name &#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;|Repository|Documentation|Keywords&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;|
|:---|:---:|:---:|:---|
| The Open Databases Integration for Materials Design - OPTIMADE | [:file_folder:](https://github.com/Materials-Consortia/OPTIMADE)                          | [:bookmark_tabs:](https://www.optimade.org/)                                                 | materials design, simulation                     |
| Standardised documentation of Simulations (MODA)               |                                                                          | [:bookmark_tabs:](https://emmc.eu/moda/)                                                     | modelling workflows                              |
| MatML                                                          |                                                                          | [:bookmark_tabs:](http://docs.oasis-open.org/materials/materials-matml-spec-pr-01.htm)       |  heat treatment, mechanical properties,chemistry |
| ColabFit Standard                                              |                                                                          | [:bookmark_tabs:](https://colabfit.org/data-standard/)                                       | atomistic simulations, ML                        |
| Electronic Structure Common Data Format (ESCDF)                | [:file_folder:](https://gitlab.com/ElectronicStructureLibrary/escdf/escdf-specifications) |                                                                           | electronic structure                             |
| Graphical Expression of Materials Data (GEMD)                  |                                                                          | [:bookmark_tabs:](https://citrineinformatics.github.io/gemd-docs/)                           | materials, processes, characterization           |
| NOMAD Metainfo                                                 |                                                                          | [:bookmark_tabs:](https://nomad-lab.eu/prod/v1/staging/docs/howto/customization/basics.html) | NOMAD, YAML                                      |


## Data Formats

|Name &#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;|Repository|Documentation|Keywords&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;|
|:---|:---:|:---:|:---|
| Physical Information File (PIF)          |                                | [:bookmark_tabs:](https://citrineinformatics.github.io/pif-documentation/index.html) | physical systems                    |
| NeXus Data Format                        | [:file_folder:](https://github.com/nexusformat) | [:bookmark_tabs:](https://www.nexusformat.org/)                                      | neutron, x-ray, and muon data       |
| NeXus-FAIRmat                            | [:file_folder:](https://www.github.com/FAIRmat-NFDI/nexus_definitions) | [:bookmark_tabs:](https://fairmat-nfdi.github.io/nexus_definitions/index.html)       | FAIRmat extension, characterization |
| Crystallographic Information File (CIF)  |                                | [:bookmark_tabs:](https://www.iucr.org/resources/cif/spec/version1.1)                | crystallography                     |
| Crystallographic Information File (CIF2) |                                | [:bookmark_tabs:](https://www.iucr.org/resources/cif/cif2)                           | crystallography                     |

## Semantic Artefacts

|Name &#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;|Repository|Documentation|Keywords&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;|
|:---|:---:|:---:|:---|
| MatWerk Ontology (MWO)                                       | [:file_folder:](https://git.rwth-aachen.de/nfdi-matwerk/ta-oms/mwo)                                    | [:bookmark_tabs:](http://purls.helmholtz-metadaten.de/mwo/)                                      | NFDI-MatWerk, resource description, MSE datasets and workflows                 |
| Elemental Multiperspective Material Ontology (EMMO)          | [:file_folder:](https://github.com/emmo-repo/EMMO)                                                     |                                                                                | EMMC, materials science                                                                           |
| Crystallographic Defect Ontology Suite (CDOS)                | [:file_folder:](https://github.com/OCDO/cdos)                                                          |                                                                                | crystallographic defects                                                       |
| Dislocation Ontology (DISO)                                  | [:file_folder:](https://github.com/Materials-Data-Science-and-Informatics/dislocation-ontology)        | [:bookmark_tabs:](https://materials-data-science-and-informatics.github.io/dislocation-ontology/) | dislocations, crystal structures                                               |
| Materials Modelling and Simulation Suite (MMSS)              | [:file_folder:](https://github.com/OCDO/mmss)                                                          |                                                                                | modelling and simulation, atomistic simulations, discrete dislocation dynamics |
| EM Glossary                                                  | [:file_folder:](https://codebase.helmholtz.cloud/em_glossary/em_glossary)                              |              [:bookmark_tabs:](https://emglossary.helmholtz-metadaten.de/)                          | electron microscopy                                                            |
| Materials Design Ontology (MDO)                              | [:file_folder:](https://github.com/LiUSemWeb/Materials-Design-Ontology)                                | [:bookmark_tabs:](https://w3id.org/mdo/full/1.0/ )                                                        | materials design, simulation, solid-state physics                              |
| Crystallographic Materials Sample Ontology (CMSO)            | [:file_folder:](https://github.com/OCDO/cmso-ontology)                                                 |                                                                                | computational sample description                                               |
| PMD Core Ontology                                            | [:file_folder:](https://github.com/materialdigital/core-ontology)                                      | [:bookmark_tabs:](https://w3id.org/pmd/co)                                                        | Platform MaterialDigital, processes, experiments                               |
| Additive Manufacturing Ontology (AMONTOLOGY)                  | [:file_folder:](https://github.com/iassouroko/AMontology)                                              |                                                                                | NIST, additive manufacturing                                                   |
| Material Science and Engineering Ontology (MSEO)             | [:file_folder:](https://github.com/Mat-O-Lab/MSEO)                                                     |                                                                                | Materials Open Lab Project (Mat-O-Lab), experiments                            |
| Materials And Molecules Basic Ontology (MAMBO)               | [:file_folder:](https://github.com/daimoners/MAMBO)                                                    |                                                                                | molecular materials                                                            |
| Chemical Methods Ontology (CHMO)                             | [:file_folder:](https://github.com/rsc-ontologies/rsc-cmo)                                             |                                                                                | chemistry, experiments                                                         |
| Characterisation Methodology Domain Ontology (CHAMEO)        | [:file_folder:](https://github.com/emmo-repo/domain-characterisation-methodology)                      |                                                                                | EMMC, materials characterization                                               |
| PRovenance Information in MAterials science (PRIMA) ontology | [:file_folder:](https://github.com/Materials-Data-Science-and-Informatics/MDMC-NEP-top-level-ontology) | [:bookmark_tabs:](https://purls.helmholtz-metadaten.de/prima/complete)                            | provenance                                                                     |

## Other Resources
|Name &#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;|Repository|Documentation|Keywords&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;|
|:---|:---:|:---:|:---|
| GitHub Repo: Metadata Schemas for Materials Science by KIT | [:file_folder:](https://github.com/kit-data-manager/Metadata-Schemas-for-Materials-Science) |                   | measurement techniques, JSON, XSD         |
| MaRDA Metadata Extractors  | [:file_folder:](https://github.com/marda-alliance/metadata_extractors)     |        | metadata extractors for material and chemical file formats|
| matportal.org                                              | [:file_folder:](https://matportal.org)                                                              |                   | ontology repository for materials science |

## Subject Agnostic
|Name &#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;|Repository|Documentation|Keywords&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;&#10240;|
|:---|:---:|:---:|:---|
| schema.org                          | [:file_folder:](https://github.com/schemaorg/schemaorg)       | [:bookmark_tabs:](https://schema.org)                                                        | resource description                   |
| Dublin Core Metadata Terms          |                                              | [:bookmark_tabs:](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/) | resource description                   |
| PROV                                |                                              | [:bookmark_tabs:](http://www.w3.org/TR/prov-overview/)                               | provenance                             |
| DataCite Metadata Schema            | [:file_folder:](https://schema.datacite.org/)                 | [:bookmark_tabs:](https://datacite-metadata-schema.readthedocs.io/en/4.5/)           | data publication, resource description |
| DCAT                                | [:file_folder:](https://github.com/w3c/dxwg/)                 | [:bookmark_tabs:](https://www.w3.org/TR/vocab-dcat/)                                 | dataset, resource description          |
| QUDT                                | [:file_folder:](https://github.com/qudt/qudt-public-repo)     | [:bookmark_tabs:](https://www.qudt.org/pages/HomePage.html)                          | software, resource description         |
| The CodeMeta Project                |                                              | [:bookmark_tabs:](https://codemeta.github.io/)                                       | software, resource description         |
| NFDI core ontology                  | [:file_folder:](https://github.com/ISE-FIZKarlsruhe/nfdicore) | [:bookmark_tabs:](https://nfdi.fiz-karlsruhe.de/ontology)                            | NFDI, resource description             |
| RO-Crate Metadata Specification 1.1 |                                              | [:bookmark_tabs:](https://www.researchobject.org/ro-crate/1.1/)                      | resource description                   |

## Acknowledgements
This work is developed at the Materials Data Science and Informatics (IAS‑9), Forschungszentrum Jülich GmbH; funded by the NFDI-MatWerk consortium (Funded by the Deutsche Forschungsgemeinschaft (DFG, German Research Foundation) under the National Research Data Infrastructure – NFDI 38/1 – project number 460247524"). 